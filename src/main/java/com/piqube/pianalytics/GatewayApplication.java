package com.piqube.pianalytics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.AntPathRequestMatcher;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.security.Principal;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;


@SpringBootApplication
@EnableRedisHttpSession(maxInactiveIntervalInSeconds=86400)
@EnableZuulProxy
@RestController
@ComponentScan
@EnableAutoConfiguration
@EnableWebSecurity
public class GatewayApplication {


    public static Logger logger = LoggerFactory.getLogger(GatewayApplication.class);

    public static void main(String[] args) {

        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    HeaderHttpSessionStrategy sessionStrategy() {
        return new HeaderHttpSessionStrategy();
    }

    @Component
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public static class CorsFilter implements Filter {

        public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) {
            HttpServletResponse response = (HttpServletResponse) res;
            HttpServletRequest request = (HttpServletRequest) req;
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
            response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Auth-Token");
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.setHeader("Access-Control-Max-Age", "3600");
            response.setHeader("X-FRAME-OPTIONS", "ALLOWALL");

            Enumeration<String> headers = request.getHeaderNames();
            String strHeaders = "";
            while (headers.hasMoreElements()) {
                String header = headers.nextElement();
                String headerValue = request.getHeader(header);
                strHeaders += header + ":" + headerValue + "\n";
            }

            String uri = request.getScheme() + "://" +
                    request.getServerName() +
                    ("http".equals(request.getScheme()) && request.getServerPort() == 80 || "https".equals(request.getScheme()) && request.getServerPort() == 443 ? "" : ":" + request.getServerPort()) +
                    request.getRequestURI() +
                    (request.getQueryString() != null ? "?" + request.getQueryString() : "");
            logger.info("API:" + request.getMethod() + " " + uri);

            logger.info("\n====HEADERS===\n" + strHeaders + "==============\n");
            if (!request.getMethod().equalsIgnoreCase("OPTIONS")) {
                try {
                    chain.doFilter(req, res);
                    if (response.getStatus() >= HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                        logger.info("Internal server error");
                    }
                } catch (IOException | ServletException e) {
                    e.printStackTrace();
                }
            }

        }

        public void init(FilterConfig filterConfig) {
        }

        public void destroy() {
        }

    }

    @Configuration
    @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
    protected static class SecurityConfiguration extends WebSecurityConfigurerAdapter {


        @Autowired
        DataSource dataSource;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .httpBasic()
                    .and()
                    .authorizeRequests()
                    .antMatchers("/index.html","/login", "/","/user/email-verify","/user/forgot-password","/user/reset-password","/signup/**","/user/page","/signup").permitAll()
                    .antMatchers(HttpMethod.POST, "/user").permitAll()
                    .antMatchers(HttpMethod.PUT, "/create-account*").permitAll()
                    .antMatchers("/user/piqube-admin/**").hasAuthority(String.valueOf(Roles.PIQUBE_ADMIN))
                    .anyRequest().authenticated()
                    .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout.html")).logoutSuccessUrl("/").invalidateHttpSession(true)
                    .and().csrf().disable();
            http.headers().frameOptions().disable();
        }


        @Override
        protected void configure(AuthenticationManagerBuilder auth)
                throws Exception {
            auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(new BCryptPasswordEncoder());
        }
    }

    @RequestMapping(value = "/session", method = RequestMethod.POST)
    public
    @ResponseBody
    SessionVO createSession(Principal user, HttpSession session) {

        logger.info("=====create session is called=====");
        SessionVO sessionVO = new SessionVO();
        sessionVO.setUsername(user.getName());
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        sessionVO.setAuthority(authorities.iterator().next().getAuthority());
        sessionVO.setTime(new Date().toString());
        sessionVO.setToken(session.getId());

        return sessionVO;
    }
}