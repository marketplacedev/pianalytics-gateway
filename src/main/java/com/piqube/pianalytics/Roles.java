package com.piqube.pianalytics;

/**
 * @author jenefar
 */
public enum Roles {
    PIQUBE_ADMIN,
    USER
}
